module.exports = { 
  paymentType: {
    cash: 'cash',
    stripe: 'stripe'
  },
  paymentStatus: {
    success: 'success',
    cancel: 'cancel',
    waiting: 'waiting'
  }
}