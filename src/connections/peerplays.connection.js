/* istanbul ignore file */
const axios = require('axios');
const config = require('config');
const RestError = require('../errors/rest.error');

const BaseConnection = require('./abstracts/base.connection');

class PeerplaysConnection extends BaseConnection {
  async register(body) {
    const signupUrl = `${config.peerid.url}auth/sign-up`;
    return axios.post(signupUrl, body).then((res) => {
      if (res.status !== 200) {
        throw new Error('Peerplays: Unknown error');
      }

      return res.data;
    }).catch((err) => {
      if(err.response.data) {
        throw new RestError(err.response.data.error, err.response.status);
      } else {
        throw err;
      }
    });
  }

  async signIn(body) {
    const signinUrl = `${config.peerid.url}auth/sign-in`;
    return axios.post(signinUrl, body).then((res) => {
      return res.data;
    }).catch((err) => {
      if(err.response.data) {
        throw new RestError(err.response.data.error, err.response.status);
      } else {
        throw err;
      }
    });
  }

  async joinApp(body) {
    const joinUrl = `${config.peerid.url}auth/token`;
    return axios.post(joinUrl, body).then((res) => {
      return res.data;
    }).catch((err) => {
      if(err.response.data) {
        throw new RestError(err.response.data.error, err.response.status);
      } else {
        throw err;
      }
    });
  }

  async refreshAccessToken(body) {
    const exchangeUrl = `${config.peerid.url}auth/refreshtoken`;
    return axios.post(exchangeUrl, body).then((res) => {
      return res.data;
    }).catch((err) => {
      if(err.response.data) {
        throw new RestError(err.response.data.error, err.response.status);
      } else {
        throw err;
      }
    });
  }

  async sendOperations(body, access_token) {
    const operationsUrl = `${config.peerid.url}app/operations`;
    return axios.post(operationsUrl, body, {
      headers: {
        'Authorization': `Bearer ${access_token}`,
        'ClientID': config.peerid.client_id
      }
    }).then((res) => {
      return res.data;
    }).catch((err) => {
      if(err.response.data) {
        throw new RestError(err.response.data.error, err.response.status);
      } else {
        throw err;
      }
    });
  }

  async getBlockchainData(params) {
    const blockchainDataUrl = `${config.peerid.url}app/blockchain-data`;
    return axios.get(blockchainDataUrl, {params}).then((res) => {
      return res.data;
    }).catch((err) => {
      if(err.response.data) {
        throw new RestError(err.response.data.error, err.response.status);
      } else {
        throw err;
      }
    });
  }

  async getLotteryWinners(startNum) {
    const res = await this.getBlockchainData({api: 'history', method: 'get_account_history',params: ['1.2.0','1.11.0',1,'1.11.0']});
    let start = startNum;
    let end = res.result.length > 0 ? +res.result[0].id.split('.')[2] : 0;
    let winners = [];

    while(start < end) {
      const {result} = await this.getBlockchainData({api: 'history', method: 'get_account_history',params: ['1.2.0',`1.11.${start}`,100,`1.11.${start + 100}`]});

      start += 100;

      const wins = result.filter((history) => {
        return history.op[0] === 121 && history.op[1].is_benefactor_reward === false;
      });

      if(wins.length > 0) {
        winners.push(...wins);
      }
    }

    return winners;
  }

  async getUserLotteries(peerplaysAccountId) {
    const {result} = await this.getBlockchainData({
      api: 'history',
      method: 'get_account_history',
      params: [peerplaysAccountId,'1.11.0',100,'1.11.0']
    });

    const histories = result.filter((history) => {
      return history.op[0] === 120;
    });

    return histories;
  }

  disconnect() {
  }

}

module.exports = PeerplaysConnection;
