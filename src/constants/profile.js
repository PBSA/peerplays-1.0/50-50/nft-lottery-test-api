module.exports = {
  userType: {
    player: 'player',
    seller: 'seller',
    admin: 'admin'
  },
  status: {
    inactive: 'inactive',
    active: 'active'
  }
};