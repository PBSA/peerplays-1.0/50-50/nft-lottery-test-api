const { model } = require('../db/models/transaction.model');
const BasePostgresRepository = require('./abstracts/base-postgres.repository');

class TransactionRepository extends BasePostgresRepository {

  constructor() {
    super(model);
  }

  async findAll() {
    return this.model.findAll();
  }

}

module.exports = TransactionRepository;
