const { model } = require('../db/models/organization.model');
const BasePostgresRepository = require('./abstracts/base-postgres.repository');

class OrganizationRepository extends BasePostgresRepository {

  constructor() {
    super(model);
  }

  async findAll() {
    return this.model.findAll();
  }

}

module.exports = OrganizationRepository;
