const config = require('config');

module.exports = {
  info: {
    title: 'NFT Lottery Test App',
    version: '1',
    description: 'APIs for NFT Lottery Test app'
  },
  host: config.swagger.host,
  apis: [
    'src/errors/*.js',
    'src/controllers/*.js',
    'src/validators/*.js',
    'src/validators/abstract/*.js',
    'src/api.module.js',
    'src/db/models/*.js'
  ],
  basePath: '/api/v1',
  schemes: config.swagger.schemes
};
